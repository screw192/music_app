import React from "react";
import {Switch, Route} from "react-router-dom";
import {Container, CssBaseline} from "@material-ui/core";

import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import NewArtist from "./containers/NewArtist/NewArtist";
import NewAlbum from "./containers/NewAlbum/NewAlbum";
import NewTrack from "./containers/NewTrack/NewTrack";
import Notifier from "./components/UI/Notistack/Notistack";


const App = () => {
  return (
    <>
      <Notifier />
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="lg">
          <Switch>
            <Route path="/" exact component={Artists} />
            <Route path="/add_artist" exact component={NewArtist} />
            <Route path="/add_album" exact component={NewAlbum} />
            <Route path="/add_track" exact component={NewTrack} />
            <Route path="/albums/:id" exact component={Albums} />
            <Route path="/tracks/:id" exact component={Tracks} />
            <Route path="/track_history" exact component={TrackHistory} />
            <Route path="/login" exact component={Login} />
            <Route path="/register" exact component={Register} />
          </Switch>
        </Container>
      </main>
    </>
  );
}

export default App;
