import {createMuiTheme} from "@material-ui/core";

const theme = createMuiTheme({
  props: {
    MuiTextField: {
      variant: "outlined",
      fullWidth: true,
    },
    MuiButton: {
      size: "large",
    }
  }
});

export default theme;