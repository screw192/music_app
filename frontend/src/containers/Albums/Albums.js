import React, {useEffect} from 'react';
import {Grid, LinearProgress, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";

import {fetchAlbums} from "../../store/actions/albumsActions";
import AlbumItem from "./AlbumItem";
import PagesHeader from "../../components/UI/PagesHeader/PagesHeader";


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const Albums = props => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const albumsData = useSelector(state => state.albums.albums);
  const albumsLoading = useSelector(state => state.albums.albumsLoading);
  const artistName = useSelector(state => state.albums.artistName);

  const artistID = props.match.params.id;

  useEffect(() => {
    dispatch(fetchAlbums(artistID));
  }, [dispatch, artistID]);

  const albums = albumsData.map(item => {
    return (
        <AlbumItem
            key={item._id}
            id={item._id}
            title={item.title}
            cover={item.cover}
            year={item.year}
            published={item.published}
            artistID={artistID}
        />
    );
  });

  return (
      <>
        {albumsLoading ? (
            <div className={classes.root}>
              <LinearProgress />
            </div>
        ) : (
            <>
              <PagesHeader
                  primaryTitle={artistName}
              />
              <Grid container spacing={4} justify="center">
                {albums}
              </Grid>
            </>
        )}
      </>
  );
};

export default Albums;