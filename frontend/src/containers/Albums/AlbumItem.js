import React from 'react';
import {Link} from "react-router-dom";
import {Card, CardContent, CardMedia, Grid, IconButton, makeStyles, Typography} from "@material-ui/core";

import {apiURL} from "../../config";
import PublishIcon from "@material-ui/icons/Publish";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import {useDispatch, useSelector} from "react-redux";
import {publishAlbum, removeAlbum} from "../../store/actions/albumsActions";


const useStyles = makeStyles({
  link: {
    textDecoration: "none",
  },
  title: {
    fontWeight: "bold",
  },
  year: {
    color: "#888888",
    fontWeight: "bold",
  },
  media: {
    height: "300px"
  },
  card: {
    position: "relative",
  },
  removeIcon: {
    padding: "6px",
    backgroundColor: "#ffffff99",
    position: "absolute",
    top: "5px",
    right: "5px",
    "&:hover": {
      backgroundColor: "#fff",
    }
  },
  publishButton: {
    padding: "6px",
    backgroundColor: "#ffffff99",
    position: "absolute",
    top: "5px",
    right: "50px",
    "&:hover": {
      backgroundColor: "#fff",
    }
  },
});

const AlbumItem = ({id, artistID, title, cover, year, published}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);

  const albumImage = `${apiURL}/${cover}`;

  let cardButtonsBlock = null;

  if (user?.role === "admin") {
    let publishButton;
    if (!published) {
      publishButton = (
          <IconButton
              onClick={e => {
                e.preventDefault();
                dispatch(publishAlbum(id, artistID));
              }}
              className={classes.publishButton}
          >
            <PublishIcon color="primary"/>
          </IconButton>
      )
    }
    cardButtonsBlock = (
        <>
          {publishButton}
          <IconButton
              onClick={e => {
                e.preventDefault();
                dispatch(removeAlbum(id, artistID));
              }}
              className={classes.removeIcon}
          >
            <DeleteForeverIcon color="secondary"/>
          </IconButton>
        </>
    );
  }

  return (
      <Grid item xs={6} md={4} lg={3}>
        <Link to={`/tracks/${id}`} className={classes.link}>
          <Card className={classes.card}>
            <CardMedia
                className={classes.media}
                image={albumImage}
            />
            <CardContent>
              <Typography className={classes.year} variant="body2">
                {year}
              </Typography>
              <Typography className={classes.title} variant="h6">
                {title}
              </Typography>
            </CardContent>
            {cardButtonsBlock}
          </Card>
        </Link>
      </Grid>
  );
};

export default AlbumItem;