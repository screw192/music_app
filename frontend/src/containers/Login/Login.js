import React, {useState} from 'react';
import {Link as RouterLink} from 'react-router-dom';
import {useDispatch} from "react-redux";
import {Avatar, Button, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import FormElement from "../../components/UI/Form/FormElement";
import {loginUser} from "../../store/actions/usersActions";
import FacebookLogin from "../../components/UI/FacebookLogin/FacebookLogin";


const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
}));

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [user, setUser] = useState({
    username: '', password: ''
  });

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setUser(prev => ({...prev, [name]: value}));
  };

  const submitFormHandler = e => {
    e.preventDefault();

    dispatch(loginUser({...user}));
  };

  return (
      <Container maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography paragraph align="center" component="h1" variant="h5">
            Sign in
          </Typography>
          <Grid container spacing={2} direction="column" component="form" onSubmit={submitFormHandler}>
            <FormElement
                label="Username"
                type="text"
                onChange={inputChangeHandler}
                name="username"
                value={user.username}
                autoComplete="new-username"
            />
            <FormElement
                label="Password"
                type="password"
                onChange={inputChangeHandler}
                name="password"
                value={user.password}
                autoComplete="new-password"
            />
            <Grid item xs>
              <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  size="large"
              >
                Sign in
              </Button>
            </Grid>
            <Grid item xs>
              <FacebookLogin
                  buttonText="Sign in"
              />
            </Grid>
            <Grid item container justify="flex-end">
              <Grid item>
                <Link component={RouterLink} variant="body2" to="/register">
                  Don't have an account yet?
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Container>
  );
};

export default Login;