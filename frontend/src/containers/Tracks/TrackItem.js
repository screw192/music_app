import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid, IconButton, makeStyles, Typography} from "@material-ui/core";
import {addToTrackHistory} from "../../store/actions/trackHistoryActions";
import PublishIcon from "@material-ui/icons/Publish";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import {publishTrack, removeTrack} from "../../store/actions/tracksActions";

const useStyles = makeStyles({
  trackNumber: {
    paddingLeft: "7px",
  },
  trackBlock: {
    position: "relative",
    cursor: "pointer",
    borderRadius: "5px",
    "&:hover": {
      backgroundColor: "#eeeeee",
      transition: "background-color 300ms ease-in-out"
    },
  },
  removeIcon: {
    padding: "6px",
    position: "absolute",
    top: "-2px",
    right: "-40px",
  },
  publishButton: {
    padding: "6px",
    position: "absolute",
    top: "-2px",
    left: "-40px",
  },
});

const TrackItem = ({id, albumID, trackNumber, title, length, published}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const userData = useSelector(state => state.users.user);

  let token = null;
  if (userData) {
    token = userData.token;
  }

  const trackID = {track: id};

  const playTrackHandler = () => {
    dispatch(addToTrackHistory(trackID, token));
  };

  let trackButtonsBlock = null;

  if (userData?.role === "admin") {
    let publishButton;
    if (!published) {
      publishButton = (
          <IconButton
              onClick={e => {
                e.preventDefault();
                dispatch(publishTrack(id, albumID));
              }}
              className={classes.publishButton}
          >
            <PublishIcon color="primary"/>
          </IconButton>
      )
    }
    trackButtonsBlock = (
        <>
          {publishButton}
          <IconButton
              onClick={e => {
                e.preventDefault();
                dispatch(removeTrack(id, albumID));
              }}
              className={classes.removeIcon}
          >
            <DeleteForeverIcon color="secondary"/>
          </IconButton>
        </>
    );
  }

  return (
      <>
        <Grid
            item
            container
            alignItems="center"
            className={classes.trackBlock}
            onClick={token ? playTrackHandler : null}
        >
          <Grid item xs className={classes.trackNumber} >
            <Typography display="inline" variant="body1">{trackNumber}</Typography>
          </Grid>
          <Grid item xs={11}>
            <Typography display="inline" variant="body1">{title}</Typography>
          </Grid>
          <Grid item xs>
            <Typography display="inline" variant="body2">{length}</Typography>
          </Grid>
          {trackButtonsBlock}
        </Grid>
      </>
  );
};

export default TrackItem;