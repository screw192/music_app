import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container, Grid} from "@material-ui/core";

import {fetchTracks} from "../../store/actions/tracksActions";
import TrackItem from "./TrackItem";
import PagesHeader from "../../components/UI/PagesHeader/PagesHeader";
import Preloader from "../../components/UI/Preloader/Preloader";


const Tracks = props => {
  const dispatch = useDispatch();
  const tracksData = useSelector(state => state.tracks.tracks);
  const artistName = useSelector(state => state.tracks.artistName);
  const albumTitle = useSelector(state => state.tracks.albumTitle);
  const tracksLoading = useSelector(state => state.tracks.tracksLoading);

  const albumID = props.match.params.id;

  useEffect(() => {
    dispatch(fetchTracks(albumID));
  }, [dispatch, albumID]);

  const tracks = tracksData.map(item => {
    return (
        <TrackItem
            key={item._id}
            id={item._id}
            trackNumber={item.trackNumber}
            title={item.title}
            length={item.length}
            published={item.published}
            albumID={albumID}
        />
    );
  });

  return (
      <>
        {tracksLoading ? (
            <Preloader/>
        ) : (
            <>
              <PagesHeader
                  primaryTitle={artistName}
                  secondaryTitle={albumTitle}
              />
              <Container maxWidth="md">
                <Grid container spacing={1} justify="center" direction="column">
                  {tracks}
                </Grid>
              </Container>
            </>
        )}
      </>
  );
};

export default Tracks;