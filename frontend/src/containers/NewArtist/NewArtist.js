import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid, Typography} from "@material-ui/core";

import ArtistForm from "../../components/ArtistForm/ArtistForm";
import {createArtist} from "../../store/actions/artistsActions";


const NewArtist = () => {
  const dispatch = useDispatch();
  const error = useSelector(state => state.artists.createArtistError);
  const loading = useSelector(state => state.artists.createArtistLoading);

  const onArtistFormSubmit = artistData => {
    dispatch(createArtist(artistData));
  };

  return (
      <Grid container direction="column" spacing={2}>
        <Grid item xs={6}>
          <Typography variant="h4">New artist</Typography>
        </Grid>
        <Grid item xs={6}>
          <ArtistForm
              onSubmit={onArtistFormSubmit}
              error={error}
              loading={loading}
          />
        </Grid>
      </Grid>
  );
};

export default NewArtist;