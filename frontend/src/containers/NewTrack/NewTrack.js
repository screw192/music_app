import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {fetchArtists} from "../../store/actions/artistsActions";
import {createTrack} from "../../store/actions/tracksActions";
import {Grid, Typography} from "@material-ui/core";
import TrackForm from "../../components/TrackForm/TrackForm";


const NewTrack = () => {
  const dispatch = useDispatch();
  const artists = useSelector(state => state.artists.artists);
  const albums = useSelector(state => state.albums.albums);
  const error = useSelector(state => state.tracks.createTrackError);
  const loading = useSelector(state => state.tracks.createTrackLoading);

  useEffect(() => {
    dispatch(fetchArtists());
  }, [dispatch]);

  const onTrackFormSubmit = trackData => {
    dispatch(createTrack(trackData));
  };

  return (
      <Grid container direction="column" spacing={2}>
        <Grid item xs={6}>
          <Typography variant="h4">New track</Typography>
        </Grid>
        <Grid item xs={6}>
          <TrackForm
              artists={artists}
              albums={albums}
              onSubmit={onTrackFormSubmit}
              error={error}
              loading={loading}
          />
        </Grid>
      </Grid>
  );
};

export default NewTrack;