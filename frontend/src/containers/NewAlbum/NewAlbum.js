import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid, Typography} from "@material-ui/core";

import {createAlbum} from "../../store/actions/albumsActions";
import AlbumForm from "../../components/AlbumForm/AlbumForm";
import {fetchArtists} from "../../store/actions/artistsActions";


const NewAlbum = () => {
  const dispatch = useDispatch();
  const artists = useSelector(state => state.artists.artists);
  const error = useSelector(state => state.albums.createAlbumError);
  const loading = useSelector(state => state.albums.createAlbumLoading);

  useEffect(() => {
    dispatch(fetchArtists());
  }, [dispatch]);

  const onAlbumFormSubmit = albumData => {
    dispatch(createAlbum(albumData));
  };

  return (
      <Grid container direction="column" spacing={2}>
        <Grid item xs={6}>
          <Typography variant="h4">New album</Typography>
        </Grid>
        <Grid item xs={6}>
          <AlbumForm
              artists={artists}
              onSubmit={onAlbumFormSubmit}
              error={error}
              loading={loading}
          />
        </Grid>
      </Grid>
  );
};

export default NewAlbum