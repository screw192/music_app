import React, {useState} from "react";
import {Link as RouterLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {Avatar, Button, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";

import FormElement from "../../components/UI/Form/FormElement";
import {registerUser} from "../../store/actions/usersActions";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import FacebookLogin from "../../components/UI/FacebookLogin/FacebookLogin";
import FileInput from "../../components/UI/Form/FileInput";


const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
}));

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const registerError = useSelector(state => state.users.registerError?.errors);

  const [user, setUser] = useState({
    username: "", password: "", displayName: "", avatarImage: "",
  });

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    if (registerError?.[name]) {
      delete registerError?.[name];
    }

    setUser(prev => ({...prev, [name]: value}));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setUser(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(user).forEach(key => {
      formData.append(key, user[key]);
    });

    dispatch(registerUser(formData));
  };

  const getFieldError = fieldName => {
    return registerError?.[fieldName]?.message;
  };

  return (
      <Container maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography paragraph align="center" component="h1" variant="h5">
            Sign up
          </Typography>
          <Grid container spacing={2} direction="column" component="form" onSubmit={submitFormHandler}>
            <FormElement
                label="Username"
                type="text"
                onChange={inputChangeHandler}
                name="username"
                value={user.username}
                autoComplete="new-username"
                error={getFieldError("username")}
            />
            <FormElement
                label="Password"
                type="password"
                onChange={inputChangeHandler}
                name="password"
                value={user.password}
                autoComplete="new-password"
                error={getFieldError("password")}
            />
            <FormElement
                label="Name to display"
                type="text"
                onChange={inputChangeHandler}
                name="displayName"
                value={user.displayName}
                error={getFieldError("displayName")}
            />
            <Grid item xs>
              <FileInput
                  name="avatarImage"
                  label="Avatar"
                  onChange={fileChangeHandler}
              />
            </Grid>
            <Grid item xs>
              <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  size="large"
              >
                Sign up
              </Button>
            </Grid>
            <Grid item xs>
              <FacebookLogin
                  buttonText="Sign up"
              />
            </Grid>
            <Grid item container justify="flex-end">
              <Grid item>
                <Link component={RouterLink} variant="body2" to="/login">
                  Already have an account?
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Container>
  );
};

export default Login;