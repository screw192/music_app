import React from 'react';
import {Grid, Typography} from "@material-ui/core";
import moment from "moment";

const TrackHistoryItem = ({artist, track, datetime}) => {
  const date = moment(datetime).format("MMM DD YYYY / HH:MM");

  return (
      <Grid item container justify="space-between">
        <Grid item>
          <Typography variant="body1" noWrap>
            <strong>{artist}</strong> - {track}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body1">
            {date}
          </Typography>
        </Grid>
      </Grid>
  );
};

export default TrackHistoryItem;