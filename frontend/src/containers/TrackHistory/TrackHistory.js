import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container, Grid} from "@material-ui/core";

import {fetchTrackHistory} from "../../store/actions/trackHistoryActions";
import TrackHistoryItem from "./TrackHistoryItem";
import PagesHeader from "../../components/UI/PagesHeader/PagesHeader";
import {historyPush} from "../../store/actions/historyActions";
import Preloader from "../../components/UI/Preloader/Preloader";


const TrackHistory = () => {
  const dispatch = useDispatch();
  const trackHistoryData = useSelector(state => state.trackHistory.trackHistory);
  const trackHistoryLoading = useSelector(state => state.trackHistory.trackHistoryLoading);
  const userData = useSelector(state => state.users.user);

  let token = null;
  if (userData) {
    token = userData.token;
  } else {
    dispatch(historyPush("/"));
  }

  useEffect(() => {
    dispatch(fetchTrackHistory(token));
  }, [dispatch, token]);

  const trackHistory = trackHistoryData.map(item => {
    return (
        <TrackHistoryItem
            key={item._id}
            artist={item.track.album.artist.name}
            track={item.track.title}
            datetime={item.datetime}
        />
    );
  });

  return (
      <>
        {trackHistoryLoading ? (
            <Preloader/>
        ) : (
            <>
              <PagesHeader
                  primaryTitle="Your track history"
              />
              <Container maxWidth="md">
                <Grid container spacing={1} justify="center" direction="column">
                  {trackHistory}
                </Grid>
              </Container>
            </>
        )}
      </>
  );
};

export default TrackHistory;