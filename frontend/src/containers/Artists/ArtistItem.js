import React from 'react';
import {Card, CardContent, CardMedia, Grid, IconButton, makeStyles, Typography} from "@material-ui/core";
import PublishIcon from '@material-ui/icons/Publish';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import {Link} from "react-router-dom";

import {apiURL} from "../../config";
import {useDispatch, useSelector} from "react-redux";
import {publishArtist, removeArtist} from "../../store/actions/artistsActions";


const useStyles = makeStyles({
  card: {
    position: "relative",
  },
  removeIcon: {
    padding: "6px",
    backgroundColor: "#ffffff99",
    position: "absolute",
    top: "5px",
    right: "5px",
    "&:hover": {
      backgroundColor: "#fff",
    }
  },
  publishButton: {
    padding: "6px",
    backgroundColor: "#ffffff99",
    position: "absolute",
    top: "5px",
    right: "50px",
    "&:hover": {
      backgroundColor: "#fff",
    }
  },
  link: {
    textDecoration: "none",
  },
  title: {
    fontWeight: "bold",
  },
  media: {
    height: "400px",
  },
});

const ArtistItem = ({id, name, image, info, published}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);

  const artistImage = `${apiURL}/${image}`;
  const artistInfo = info ? info : "Info not available";


  let cardButtonsBlock = null;

  if (user?.role === "admin") {
    let publishButton;
    if (!published) {
      publishButton = (
          <IconButton
              onClick={e => {
                e.preventDefault();
                dispatch(publishArtist(id));
              }}
              className={classes.publishButton}
          >
            <PublishIcon color="primary"/>
          </IconButton>
      );
    }

    cardButtonsBlock = (
        <>
          {publishButton}
          <IconButton
              onClick={e => {
                e.preventDefault();
                dispatch(removeArtist(id));
              }}
              className={classes.removeIcon}
          >
            <DeleteForeverIcon color="secondary"/>
          </IconButton>
        </>
    );
  }

  return (
      <Grid item xs={6} md={4} lg={3}>
        <Link to={`/albums/${id}`} className={classes.link}>
          <Card className={classes.card}>
            <CardMedia
                className={classes.media}
                image={artistImage}
                title={artistInfo}
            />
            <CardContent>
              <Typography className={classes.title} variant="h5">
                {name}
              </Typography>
            </CardContent>
            {cardButtonsBlock}
          </Card>
        </Link>
      </Grid>
  );
};

export default ArtistItem;