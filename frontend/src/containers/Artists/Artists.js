import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Grid} from "@material-ui/core";

import {fetchArtists} from "../../store/actions/artistsActions";
import ArtistItem from "./ArtistItem";
import Preloader from "../../components/UI/Preloader/Preloader";


const Artists = () => {
  const dispatch = useDispatch();
  const artistsData = useSelector(state => state.artists.artists);
  const artistsLoading = useSelector(state => state.artists.artistsLoading);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    dispatch(fetchArtists());
  }, [dispatch, user]);

  const artists = artistsData.map(item => {
    return (
      <ArtistItem
          key={item._id}
          id={item._id}
          name={item.name}
          image={item.photo}
          info={item.info}
          published={item.published}
      />
    );
  });

  return (
      <>
        {artistsLoading ? (
              <Preloader/>
            ) : (
              <Grid container spacing={4} justify="center">
                {artists}
              </Grid>
        )}
      </>
  );
};

export default Artists;