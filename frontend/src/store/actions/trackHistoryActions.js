import axiosApi from "../../axiosApi";

export const FETCH_TRACK_HISTORY_REQUEST = "FETCH_TRACK_HISTORY_REQUEST";
export const FETCH_TRACK_HISTORY_SUCCESS = "FETCH_TRACK_HISTORY_SUCCESS";
export const FETCH_TRACK_HISTORY_FAILURE = "FETCH_TRACK_HISTORY_FAILURE";

export const ADD_TO_TRACK_HISTORY_REQUEST = "ADD_TO_TRACK_HISTORY_REQUEST";
export const ADD_TO_TRACK_HISTORY_SUCCESS = "ADD_TO_TRACK_HISTORY_SUCCESS";
export const ADD_TO_TRACK_HISTORY_FAILURE = "ADD_TO_TRACK_HISTORY_FAILURE";

export const fetchTrackHistoryRequest = () => ({type: FETCH_TRACK_HISTORY_REQUEST});
export const fetchTrackHistorySuccess = trackHistoryData => ({type: FETCH_TRACK_HISTORY_SUCCESS, trackHistoryData});
export const fetchTrackHistoryFailure = error => ({type: FETCH_TRACK_HISTORY_FAILURE, error});

export const fetchTrackHistory = token => {
  return async dispatch => {
    try {
      dispatch(fetchTrackHistoryRequest());

      const response = await axiosApi.get("/track_history", {headers: {Authorization: token}});
      dispatch(fetchTrackHistorySuccess(response.data));
    } catch (e) {
      dispatch(fetchTrackHistoryFailure(e));
    }
  };
};

export const addToTrackHistoryRequest = () => ({type: ADD_TO_TRACK_HISTORY_REQUEST});
export const addToTrackHistorySuccess = () => ({type: ADD_TO_TRACK_HISTORY_SUCCESS});
export const addToTrackHistoryFailure = () => ({type: ADD_TO_TRACK_HISTORY_FAILURE});

export const addToTrackHistory = (trackData, token) => {
  return async dispatch => {
    try {
      dispatch(addToTrackHistoryRequest());

      await axiosApi.post("/track_history", trackData, {headers: {Authorization: token}});
      dispatch(addToTrackHistorySuccess());
    } catch (e) {
      dispatch(addToTrackHistoryFailure());
    }
  };
};