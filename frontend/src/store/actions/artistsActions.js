import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_ARTISTS_REQUEST = "FETCH_ARTISTS_REQUEST";
export const FETCH_ARTISTS_SUCCESS = "FETCH_ARTISTS_SUCCESS";
export const FETCH_ARTISTS_FAILURE = "FETCH_ARTISTS_FAILURE";

export const CREATE_ARTIST_REQUEST = "CREATE_ARTIST_REQUEST";
export const CREATE_ARTIST_SUCCESS = "CREATE_ARTIST_SUCCESS";
export const CREATE_ARTIST_FAILURE = "CREATE_ARTIST_FAILURE";

export const PUBLISH_ARTIST_SUCCESS = "PUBLISH_ARTIST_SUCCESS";

export const REMOVE_ARTIST_SUCCESS = "REMOVE_ARTIST_SUCCESS";

export const fetchArtistsRequest = () => ({type: FETCH_ARTISTS_REQUEST});
export const fetchArtistsSuccess = artistsData => ({type: FETCH_ARTISTS_SUCCESS, artistsData});
export const fetchArtistsFailure = error => ({type:FETCH_ARTISTS_FAILURE, error});

export const createArtistRequest = () => ({type: CREATE_ARTIST_REQUEST});
export const createArtistSuccess = () => ({type: CREATE_ARTIST_SUCCESS});
export const createArtistFailure = error => ({type: CREATE_ARTIST_FAILURE, error});

export const publishArtistSuccess = () => ({type: PUBLISH_ARTIST_SUCCESS});

export const removeArtistSuccess = () => ({type: REMOVE_ARTIST_SUCCESS});

export const fetchArtists = () => {
  return async dispatch => {
    try {
      dispatch(fetchArtistsRequest());

      const response = await axiosApi.get("/artists");
      dispatch(fetchArtistsSuccess(response.data));
    } catch (e) {
      dispatch(fetchArtistsFailure(e));
    }
  };
};

export const createArtist = artistData => {
  return async dispatch => {
    try {
      dispatch(createArtistRequest());

      await axiosApi.post("/artists", artistData);
      dispatch(createArtistSuccess());

      dispatch(historyPush("/"));
    } catch (e) {
      dispatch(createArtistFailure(e.response.data));
    }
  };
};

export const publishArtist = artistID => {
  return async dispatch => {
    await axiosApi.patch(`/artists/${artistID}`, {published: true});

    dispatch(publishArtistSuccess());
    dispatch(fetchArtists());
  };
};

export const removeArtist = artistID => {
  return async dispatch => {
    await axiosApi.delete(`/artists/${artistID}`);

    dispatch(removeArtistSuccess());
    dispatch(fetchArtists());
  };
};