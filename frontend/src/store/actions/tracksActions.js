import axiosApi from "../../axiosApi";

export const FETCH_TRACKS_REQUEST = "FETCH_TRACKS_REQUEST";
export const FETCH_TRACKS_SUCCESS = "FETCH_TRACKS_SUCCESS";
export const FETCH_TRACKS_FAILURE = "FETCH_TRACKS_FAILURE";

export const CREATE_TRACK_REQUEST = "CREATE_TRACK_REQUEST";
export const CREATE_TRACK_SUCCESS = "CREATE_TRACK_SUCCESS";
export const CREATE_TRACK_FAILURE = "CREATE_TRACK_FAILURE";

export const PUBLISH_TRACK_SUCCESS = "PUBLISH_TRACK_SUCCESS";

export const REMOVE_TRACK_SUCCESS = "REMOVE_TRACK_SUCCESS";

export const fetchTracksRequest = () => ({type: FETCH_TRACKS_REQUEST});
export const fetchTracksSuccess = tracksData => ({type: FETCH_TRACKS_SUCCESS, tracksData});
export const fetchTracksFailure = error => ({type: FETCH_TRACKS_FAILURE, error});

export const createTrackRequest = () => ({type: CREATE_TRACK_REQUEST});
export const createTrackSuccess = () => ({type: CREATE_TRACK_SUCCESS});
export const createTrackFailure = error => ({type: CREATE_TRACK_FAILURE, error});

export const publishTrackSuccess = () => ({type: PUBLISH_TRACK_SUCCESS});

export const removeTrackSuccess = () => ({type: REMOVE_TRACK_SUCCESS});

export const fetchTracks = albumID => {
  return async dispatch => {
    try {
      dispatch(fetchTracksRequest());

      const response = await axiosApi.get(`/tracks?album=${albumID}`);
      dispatch(fetchTracksSuccess(response.data));
    } catch (e) {
      dispatch(fetchTracksFailure(e));
    }
  };
};

export const createTrack = trackData => {
  return async dispatch => {
    try {
      dispatch(createTrackRequest());

      await axiosApi.post("/tracks", trackData);
      dispatch(createTrackSuccess());
    } catch (e) {
      dispatch(createTrackFailure(e.response.data));
    }
  };
};

export const publishTrack = (trackID, albumID) => {
  return async dispatch => {
    await axiosApi.patch(`/tracks/${trackID}`, {published: true});

    dispatch(publishTrackSuccess());
    dispatch(fetchTracks(albumID));
  };
};

export const removeTrack = (trackID, albumID) => {
  return async dispatch => {
    await axiosApi.delete(`/tracks/${trackID}`);

    dispatch(removeTrackSuccess());
    dispatch(fetchTracks(albumID));
  };
};