import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_ALBUMS_REQUEST = "FETCH_ALBUMS_REQUEST";
export const FETCH_ALBUMS_SUCCESS = "FETCH_ALBUMS_SUCCESS";
export const FETCH_ALBUMS_FAILURE = "FETCH_ALBUMS_FAILURE";

export const CREATE_ALBUM_REQUEST = "CREATE_ALBUM_REQUEST";
export const CREATE_ALBUM_SUCCESS = "CREATE_ALBUM_SUCCESS";
export const CREATE_ALBUM_FAILURE = "CREATE_ALBUM_FAILURE";

export const PUBLISH_ALBUM_SUCCESS = "PUBLISH_ALBUM_SUCCESS";

export const REMOVE_ALBUM_SUCCESS = "REMOVE_ALBUM_SUCCESS";

export const fetchAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
export const fetchAlbumsSuccess = albumsData => ({type: FETCH_ALBUMS_SUCCESS, albumsData});
export const fetchAlbumsFailure = error => ({type:FETCH_ALBUMS_FAILURE, error});

export const createAlbumRequest = () => ({type: CREATE_ALBUM_REQUEST});
export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});
export const createAlbumFailure = error => ({type: CREATE_ALBUM_FAILURE, error});

export const publishAlbumSuccess = () => ({type: PUBLISH_ALBUM_SUCCESS});

export const removeAlbumSuccess = () => ({type: REMOVE_ALBUM_SUCCESS});

export const fetchAlbums = artistID => {
  return async dispatch => {
    try {
      dispatch(fetchAlbumsRequest());

      let response;
      if (artistID) {
        response = await axiosApi.get(`/albums?artist=${artistID}`);
      } else {
        response = await axiosApi.get("/albums");
      }
      dispatch(fetchAlbumsSuccess(response.data));
    } catch (e) {
      dispatch(fetchAlbumsFailure(e));
    }
  };
};

export const createAlbum = albumData => {
  return async dispatch => {
    try {
      dispatch(createAlbumRequest());

      await axiosApi.post("/albums", albumData);
      dispatch(createAlbumSuccess());

      dispatch(historyPush("/"));
    } catch (e) {
      dispatch(createAlbumFailure(e.response.data));
    }
  };
};

export const publishAlbum = (albumID, artistID) => {
  return async dispatch => {
    await axiosApi.patch(`/albums/${albumID}`, {published: true});

    dispatch(publishAlbumSuccess());
    dispatch(fetchAlbums(artistID));
  };
};

export const removeAlbum = (albumID, artistID) => {
  return async dispatch => {
    await axiosApi.delete(`/albums/${albumID}`);

    dispatch(removeAlbumSuccess());
    dispatch(fetchAlbums(artistID));
  };
};