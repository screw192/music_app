import {
  CREATE_ARTIST_FAILURE,
  CREATE_ARTIST_REQUEST,
  CREATE_ARTIST_SUCCESS,
  FETCH_ARTISTS_FAILURE,
  FETCH_ARTISTS_REQUEST,
  FETCH_ARTISTS_SUCCESS
} from "../actions/artistsActions";

const initialState = {
  artists: [],
  artistsLoading: false,
  artistsError: false,
  createArtistLoading: false,
  createArtistError: null,
};

const artistsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARTISTS_REQUEST:
      return {...state, artistsLoading: true};
    case FETCH_ARTISTS_SUCCESS:
      return {...state, artistsLoading: false, artists: action.artistsData};
    case FETCH_ARTISTS_FAILURE:
      return {...state, artistsLoading: false, artistsError: true};
    case CREATE_ARTIST_REQUEST:
      return {...state, createArtistLoading: true};
    case CREATE_ARTIST_SUCCESS:
      return {...state, createArtistLoading: false};
    case  CREATE_ARTIST_FAILURE:
      return {...state, createArtistLoading: false, createArtistError: action.error};
    default:
      return state;
  }
};

export default artistsReducer;