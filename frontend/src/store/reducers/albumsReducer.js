import {
  CREATE_ALBUM_FAILURE,
  CREATE_ALBUM_REQUEST, CREATE_ALBUM_SUCCESS,
  FETCH_ALBUMS_FAILURE,
  FETCH_ALBUMS_REQUEST,
  FETCH_ALBUMS_SUCCESS
} from "../actions/albumsActions";

const initialState = {
  artistName: "",
  albums: [],
  albumsLoading: false,
  albumsError: false,
  createAlbumLoading: false,
  createAlbumError: null,
};

const albumsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALBUMS_REQUEST:
      return {...state, albumsLoading: true};
    case FETCH_ALBUMS_SUCCESS:
      return {
        ...state,
        albumsLoading: false,
        artistName: action.albumsData.artistName,
        albums: action.albumsData.albums,
      };
    case FETCH_ALBUMS_FAILURE:
      return {...state, albumsLoading: false, albumsError: true};
    case CREATE_ALBUM_REQUEST:
      return {...state, createAlbumLoading: true};
    case CREATE_ALBUM_SUCCESS:
      return {...state, createAlbumLoading: false};
    case  CREATE_ALBUM_FAILURE:
      return {...state, createAlbumLoading: false, createAlbumError: action.error};
    default:
      return state;
  }
};

export default albumsReducer;