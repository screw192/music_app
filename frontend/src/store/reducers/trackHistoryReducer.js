import {
  FETCH_TRACK_HISTORY_FAILURE,
  FETCH_TRACK_HISTORY_REQUEST,
  FETCH_TRACK_HISTORY_SUCCESS
} from "../actions/trackHistoryActions";

const initialState = {
  trackHistory: [],
  trackHistoryLoading: false,
  trackHistoryError: null,
};

const trackHistoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TRACK_HISTORY_REQUEST:
      return {...state, trackHistoryLoading: true};
    case FETCH_TRACK_HISTORY_SUCCESS:
      return {...state, trackHistoryLoading: false, trackHistory: action.trackHistoryData};
    case FETCH_TRACK_HISTORY_FAILURE:
      return {...state, trackHistoryLoading: false, trackHistoryError: action.error};
    default:
      return state;
  }
};

export default trackHistoryReducer;