import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";

import FormElement from "../UI/Form/FormElement";
import FileInput from "../UI/Form/FileInput";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";


const AlbumForm = ({artists, onSubmit, loading, error}) => {
  const [album, setAlbum] = useState({
    title: "",
    year: "",
    cover: "",
    artist: "",
  });

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(album).forEach(key => {
      formData.append(key, album[key]);
    });

    onSubmit(formData);
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;

    setAlbum(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setAlbum(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  return (
      <form onSubmit={submitFormHandler} noValidate>
        <Grid container direction="column" spacing={2}>
          <FormElement
              required
              select
              label="Artist"
              name="artist"
              value={album.artist}
              onChange={inputChangeHandler}
              options={artists}
              optionsKey="name"
              error={getFieldError("artist")}
          />
          <FormElement
              required
              label="Album title"
              name="title"
              value={album.title}
              onChange={inputChangeHandler}
              error={getFieldError("title")}
          />
          <FormElement
              required
              label="Release year"
              name="year"
              value={album.year}
              onChange={inputChangeHandler}
              error={getFieldError("year")}
          />
          <Grid item xs>
            <FileInput
                name="cover"
                label="Album cover"
                onChange={fileChangeHandler}
                error={getFieldError("cover")}
            />
          </Grid>
          <Grid item xs>
            <ButtonWithProgress
                type="submit"
                color="primary"
                variant="contained"
                loading={loading}
                disabled={loading}
            >
              Add artist
            </ButtonWithProgress>
          </Grid>
        </Grid>
      </form>
  );
};

export default AlbumForm;