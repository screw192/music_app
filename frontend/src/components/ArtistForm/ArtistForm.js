import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";

import FormElement from "../UI/Form/FormElement";
import FileInput from "../UI/Form/FileInput";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";


const ArtistForm = ({onSubmit, loading, error}) => {
  const [artist, setArtist] = useState({
    name: "",
    photo: "",
    info: ""
  });

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(artist).forEach(key => {
      formData.append(key, artist[key]);
    });

    onSubmit(formData);
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;

    setArtist(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setArtist(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };
  
  return (
      <form onSubmit={submitFormHandler} noValidate>
        <Grid container direction="column" spacing={2}>
          <FormElement
              required
              label="Artist's name"
              name="name"
              value={artist.name}
              onChange={inputChangeHandler}
              error={getFieldError("name")}
          />
          <FormElement
              multiline
              rows={5}
              label="Info"
              name="info"
              value={artist.info}
              onChange={inputChangeHandler}
              error={getFieldError("description")}
          />

          <Grid item xs>
            <FileInput
                name="photo"
                label="Photo"
                onChange={fileChangeHandler}
                error={getFieldError("photo")}
            />
          </Grid>
          <Grid item xs>
            <ButtonWithProgress
                type="submit"
                color="primary"
                variant="contained"
                loading={loading}
                disabled={loading}
            >
              Add artist
            </ButtonWithProgress>
          </Grid>
        </Grid>
      </form>
  );
};

export default ArtistForm;