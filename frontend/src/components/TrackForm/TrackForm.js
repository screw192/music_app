import React, {useEffect, useState} from 'react';
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";

import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {fetchAlbums} from "../../store/actions/albumsActions";


const trackInitialState = {
  title: "",
  trackNumber: "",
  length: "",
  artist: "",
  album: "",
};

const TrackForm = ({artists, albums, onSubmit, loading, error}) => {
  const dispatch = useDispatch();

  const [track, setTrack] = useState({...trackInitialState});

  useEffect(() => {
    dispatch(fetchAlbums(track.artist));
  },[dispatch, track.artist]);

  const submitFormHandler = async e => {
    e.preventDefault();

    await onSubmit(track);
    setTrack({...trackInitialState});
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;

    setTrack(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  return (
      <form onSubmit={submitFormHandler} noValidate>
        <Grid container direction="column" spacing={2}>
          <FormElement
              select
              label="Artist"
              name="artist"
              value={track.artist}
              onChange={inputChangeHandler}
              options={artists}
              optionsKey="name"
          />
          <FormElement
              required
              select
              label="Album"
              name="album"
              value={track.album}
              onChange={inputChangeHandler}
              options={albums}
              optionsKey="title"
              error={getFieldError("album")}
          />
          <FormElement
              required
              label="Track #"
              name="trackNumber"
              value={track.trackNumber}
              onChange={inputChangeHandler}
              error={getFieldError("trackNumber")}
          />
          <FormElement
              required
              label="Title"
              name="title"
              value={track.title}
              onChange={inputChangeHandler}
              error={getFieldError("title")}
          />
          <FormElement
              required
              label="Length"
              name="length"
              value={track.length}
              onChange={inputChangeHandler}
              error={getFieldError("length")}
          />
          <Grid item xs>
            <ButtonWithProgress
                type="submit"
                color="primary"
                variant="contained"
                loading={loading}
                disabled={loading}
            >
              Add track
            </ButtonWithProgress>
          </Grid>
        </Grid>
      </form>
  );
};

export default TrackForm;