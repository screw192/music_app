import React, {useRef, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles({
  input: {
    display: 'none'
  },
  button: {
    height: "56px",
  }
});

const FileInput = ({onChange, name, label, error}) => {
  const classes = useStyles();
  const inputRef = useRef();

  const [filename, setFilename] = useState('');

  const activateInput = () => {
    inputRef.current.click();
  };

  const onFileChange = e => {
    if (e.target.files[0]) {
      setFilename(e.target.files[0].name);
    } else {
      setFilename('');
    }

    onChange(e);
  };

  return (
      <>
        <input
            type="file"
            name={name}
            className={classes.input}
            onChange={onFileChange}
            ref={inputRef}
        />
        <Grid container spacing={2} alignItems="flex-start">
          <Grid item xs>
            <TextField
                variant="outlined"
                disabled
                fullWidth
                label={label}
                value={filename}
                onClick={activateInput}
                error={Boolean(error)}
                helperText={error}
            />
          </Grid>
          <Grid item>
            <Button
                className={classes.button}
                variant="contained"
                onClick={activateInput}
            >
              Browse
            </Button>
          </Grid>
        </Grid>
      </>
  );
};

export default FileInput;