import React from 'react';
import {makeStyles, Typography} from "@material-ui/core";


const useStyles = makeStyles({
  primaryTitle: {
    fontWeight: "bold",
    paddingBottom: "10px",
    paddingLeft: "10px",
    borderBottom: "2px solid #cccccc",
  },
  secondaryTitle: {
    color: "#777777",
  }
})

const PagesHeader = ({primaryTitle, secondaryTitle = null}) => {
  const classes = useStyles();

  let breaker;
  let additional;
  if (secondaryTitle) {
    breaker = " / ";
    additional = <span className={classes.secondaryTitle}>{secondaryTitle}</span>;
  }

  return (
      <Typography className={classes.primaryTitle} variant="h5" paragraph>
        {primaryTitle}{breaker}{additional}
      </Typography>
  );
};

export default PagesHeader;