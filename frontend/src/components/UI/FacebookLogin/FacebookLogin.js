import React from "react";
import {useDispatch} from "react-redux";
import FacebookLoginButton from "react-facebook-login/dist/facebook-login-render-props";
import FacebookIcon from "@material-ui/icons/Facebook";
import {Button} from "@material-ui/core";

import {facebookLogin} from "../../../store/actions/usersActions";


const FacebookLogin = ({buttonText}) => {
  const dispatch = useDispatch();

  const facebookResponse = response => {
    if (response.id) {
      dispatch(facebookLogin(response));
    }
  }

  return (
      <FacebookLoginButton
          appId="461824121781177"
          fields="name,picture"
          render={props => (
              <Button
                  fullWidth
                  color="primary"
                  variant="outlined"
                  startIcon={<FacebookIcon/>}
                  onClick={props.onClick}
              >
                {buttonText} with Facebook
              </Button>
          )}
          callback={facebookResponse}
      />
  );
};

export default FacebookLogin;