import React, {useEffect, useRef} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {
  Avatar,
  Button,
  ClickAwayListener,
  Grow,
  makeStyles,
  MenuItem,
  MenuList,
  Paper,
  Popper
} from "@material-ui/core";

import {logoutUser} from "../../../../store/actions/usersActions";
import {apiURL} from "../../../../config";


const useStyles = makeStyles({
  avatar: {
    backgroundColor: "#ffffff50",
    marginRight: "7px",
    width: "30px",
    height: "30px",
    border: "1px solid #ffffff",
  },
});

const UserMenu = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const displayName = useSelector(state => state.users.user.displayName);
  const avatarImage = useSelector(state => state.users.user.avatarImage);

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const handleListKeyDown = event => {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  };

  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  let avatar;
  if (avatarImage) {
    if (avatarImage.startsWith("https://")) {
      avatar = avatarImage;
    } else {
      avatar = `${apiURL}/${avatarImage}`;
    }
  }

  return (
      <>
        <Button
            color="inherit"
            ref={anchorRef}
            onClick={handleToggle}
        >
          <Avatar
              className={classes.avatar}
              variant="rounded"
              alt={displayName}
              src={avatar}
          />
          {displayName}!
        </Button>
        <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
          {({ TransitionProps, placement }) => (
              <Grow
                  {...TransitionProps}
                  style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={handleClose}>
                    <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                      <MenuItem onClick={handleClose} component={Link} to="/track_history">Track history</MenuItem>
                      <MenuItem onClick={handleClose} component={Link} to="/add_artist">Add artist</MenuItem>
                      <MenuItem onClick={handleClose} component={Link} to="/add_album">Add album</MenuItem>
                      <MenuItem onClick={handleClose} component={Link} to="/add_track">Add track</MenuItem>
                      <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
          )}
        </Popper>
      </>
  );
};

export default UserMenu;