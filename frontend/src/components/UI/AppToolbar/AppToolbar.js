import React from 'react';
import {AppBar, Grid, makeStyles, Toolbar} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import AnonymousMenu from "./Menu/AnonymousMenu";
import UserMenu from "./Menu/UserMenu";


const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    marginBottom: "30px",
  },
  homeLink: {
    fontSize: "34px",
    textDecoration: "none",
    fontWeight: "bold",
    color: "#ffffff"
  },
  toolbar: {
    paddingTop: "10px",
    paddingBottom: "10px",
  }
}));

const AppToolbar = () => {
  const classes = useStyles();
  const user = useSelector(state => state.users.user);

  return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar className={classes.toolbar} variant="dense">
            <Grid container justify="space-between" alignItems="center">
              <Grid item>
                <Link to="/" className={classes.homeLink}>
                  Shpotimfy
                </Link>
              </Grid>
              <Grid item>
                {user ? (
                    <UserMenu/>
                ) : (
                    <AnonymousMenu/>
                )}
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </div>
  );
};

export default AppToolbar;