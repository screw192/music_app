const mongoose = require("mongoose");

const AlbumSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  year: {
    type: String,
    required: true,
  },
  cover: {
    type: String,
    required: true,
  },
  artist: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Artist",
    required: true,
  },
  published: {
    type: Boolean,
    default: false,
    required: true,
  },
});

const Album = mongoose.model("Album", AlbumSchema);
module.exports = Album;