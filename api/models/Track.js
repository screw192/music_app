const mongoose = require("mongoose");

const TrackSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  trackNumber: {
    type: Number,
    required: true,
  },
  length: {
    type: String,
    required: true,
  },
  album: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Album",
    required: true,
  },
  published: {
    type: Boolean,
    default: false,
    required: true,
  },
});

const Track = mongoose.model("Track", TrackSchema);
module.exports = Track;