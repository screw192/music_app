const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Artist = require("./models/Artist");
const Album = require("./models/Album");
const Track = require("./models/Track");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  await User.create({
    username: "admin",
    displayName: "Yoba uulu Admin",
    avatarImage: "fixtures/avatars/admin.png",
    password: "admin",
    token: nanoid(),
    role: "admin",
  }, {
    username: "user",
    displayName: "Anon Semyon",
    avatarImage: "fixtures/avatars/user.png",
    password: "user",
    token: nanoid(),
    role: "user",
  });

  const [asapFerg, denzelCurry, future] = await Artist.create({
    name: "A$AP Ferg",
    photo: "fixtures/artists/asap_ferg.jpeg",
    info: "Darold Durard Brown Ferguson Jr. (born October 20, 1988), known professionally as ASAP Ferg (stylized as A$AP Ferg), is an American rapper, singer, and songwriter from New York City's Harlem neighborhood. Aside from his solo career, he is a member of the hip hop collective A$AP Mob, from which he adopted a record deal with Polo Grounds and RCA, the same labels that helped launch A$AP Worldwide. Two years prior, Ferg's A$AP Mob cohorts A$AP Rocky and A$AP Yams, negotiated their own respective deal in 2011. His debut studio album Trap Lord was released on August 20, 2013, and was met with generally positive reviews. On April 22, 2016, Ferg released his second studio album, Always Strive and Prosper. On August 18, 2017, Ferg released his third studio album, Still Striving. His debut EP Floor Seats was released on August 16, 2019.",
    published: true,
  }, {
    name: "Denzel Curry",
    photo: "fixtures/artists/denzel_curry.jpeg",
    info: "Denzel Rae Don Curry (born February 16, 1995) is an American rapper, songwriter, and composer. Raised in Carol City, Florida, Curry started rapping while in sixth grade and began working on his first mixtape in 2011, when he was attending school. Influenced by underground Florida rapper SpaceGhostPurrp, the mixtape was later featured on Purrp's social media, giving Curry local attention and resulting in Curry joining Purrp's hip-hop collective, Raider Klan.\n\nCurry left Raider Klan in 2013, releasing his debut studio album, Nostalgic 64, in September of that year, while still in high school. He has gone on to release four more projects, 32 Zel/Planet Shrooms on June 9, 2015, Imperial on March 9, 2016, and Ta13oo on July 27, 2018, which debuted at number 28 on the Billboard 200 chart. His fourth studio album, Zuu, was released on May 31, 2019, and a collaborative project with Kenny Beats, Unlocked, was released on February 7, 2020. All of his projects have garnered significant acclaim.",
    published: true,
  }, {
    name: "Future",
    photo: "fixtures/artists/future.jpeg",
    info: "Nayvadius DeMun Wilburn (born November 20, 1983), better known by his stage name Future, is an American rapper, singer, songwriter, and record producer. Born and raised in Atlanta, Georgia, he first became involved in music as part of the Dungeon Family collective, where he was nicknamed \"the Future\". After amassing a series of mixtapes between 2010 and 2011, Future signed a major record label deal with Epic Records and Rocko's A1 Recordings, which helped launch Future's own label imprint, Freebandz. He subsequently released his debut album, Pluto, in April 2012 to positive reviews. Future's second album, Honest, was released in April 2014, surpassing his debut on the album charts.\n\nBetween late 2014 and early 2015, he released a trio of mixtapes to critical praise: Monster (2014), Beast Mode (2015), and 56 Nights (2015). His next releases, DS2 (2015), What a Time to Be Alive (2015, in collaboration with Drake), Evol (2016), Future (2017), Hndrxx (2017), The Wizrd (2019), and High Off Life (2020) all debuted at number one on the U.S. Billboard 200. Future and Hndrxx made him the first artist since 2014 to debut two albums in consecutive weeks atop of that chart. Future has also released several singles certified gold or higher by the RIAA, including \"Turn On the Lights\", \"Move That Dope\", \"Fuck Up Some Commas\", \"Where Ya At\", \"Jumpman\", \"Low Life\", \"Mask Off\", and \"Life Is Good\", with the latter becoming his highest-charting single, and included on his eighth studio album, High Off Life.",
    published: false,
  });

  const [trapLord, stillStriving, zuu, imperial, evol] = await Album.create({
    title: "Trap Lord",
    year: "2013",
    cover: "fixtures/albums/trap_lord.jpeg",
    artist: asapFerg,
    published: true,
  }, {
    title: "Still Striving",
    year: "2017",
    cover: "fixtures/albums/still_striving.jpeg",
    artist: asapFerg,
    published: true,
  }, {
    title: "ZUU",
    year: "2016",
    cover: "fixtures/albums/zuu.jpeg",
    artist: denzelCurry,
    published: true,
  }, {
    title: "Imperial",
    year: "2019",
    cover: "fixtures/albums/imperial.jpeg",
    artist: denzelCurry,
    published: false,
  }, {
    title: "Evol",
    year: "2016",
    cover: "fixtures/albums/evol.jpeg",
    artist: future,
    published: false,
  });

  await Track.create({
    title: "Let It Go",
    trackNumber: 1,
    length: "4:42",
    album: trapLord,
    published: true,
  }, {
    title: "Shabba (feat. A$AP Rocky)",
    trackNumber: 2,
    length: "4:35",
    album: trapLord,
    published: true,
  }, {
    title: "Lord (feat. Bone Thugs-n-Harmony)",
    trackNumber: 3,
    length: "5:17",
    album: trapLord,
    published: true,
  }, {
    title: "Hood Pope",
    trackNumber: 4,
    length: "3:30",
    album: trapLord,
    published: false,
  }, {
    title: "Fergivicious",
    trackNumber: 5,
    length: "3:50",
    album: trapLord,
    published: false,
  }, {
    title: "Trap And A Dream (feat. Meek Mill)",
    trackNumber: 1,
    length: "3:09",
    album: stillStriving,
    published: true,
  }, {
    title: "Rubber Band Man (feat. Cam'ron)",
    trackNumber: 2,
    length: "3:25",
    album: stillStriving,
    published: true,
  }, {
    title: "Olympian (feat. Dave East)",
    trackNumber: 3,
    length: "3:31",
    album: stillStriving,
    published: true,
  }, {
    title: "Aww Yeah (feat. Lil Yachty)",
    trackNumber: 4,
    length: "3:14",
    album: stillStriving,
    published: true,
  }, {
    title: "What Do You Do (feat. NAV)",
    trackNumber: 5,
    length: "2:56",
    album: stillStriving,
    published: true,
  }, {
    title: "ZUU",
    trackNumber: 1,
    length: "2:06",
    album: zuu,
    published: true,
  }, {
    title: "RICKY",
    trackNumber: 2,
    length: "2:27",
    album: zuu,
    published: true,
  }, {
    title: "WISH FEAT. KIDDO MARV",
    trackNumber: 3,
    length: "3:12",
    album: zuu,
    published: false,
  }, {
    title: "BIRDZ FEAT. RICK ROSS",
    trackNumber: 4,
    length: "3:24",
    album: zuu,
    published: false,
  }, {
    title: "AUTOMATIC",
    trackNumber: 5,
    length: "3:02",
    album: zuu,
    published: false,
  }, {
    title: "ULT",
    trackNumber: 1,
    length: "4:07",
    album: imperial,
    published: false,
  }, {
    title: "Gook",
    trackNumber: 2,
    length: "2:46",
    album: imperial,
    published: false,
  }, {
    title: "Sick & Tired",
    trackNumber: 3,
    length: "4:02",
    album: imperial,
    published: false,
  }, {
    title: "Knotty Head",
    trackNumber: 4,
    length: "4:28",
    album: imperial,
    published: false,
  }, {
    title: "Me Now",
    trackNumber: 5,
    length: "4:39",
    album: imperial,
    published: false,
  }, {
    title: "Ain't No Time",
    trackNumber: 1,
    length: "3:22",
    album: evol,
    published: false,
  }, {
    title: "In Her Mouth",
    trackNumber: 2,
    length: "3:12",
    album: evol,
    published: false,
  }, {
    title: "Maybach",
    trackNumber: 3,
    length: "3:40",
    album: evol,
    published: false,
  }, {
    title: "Xanny Family",
    trackNumber: 4,
    length: "3:05",
    album: evol,
    published: false,
  }, {
    title: "Lil Haiti Baby",
    trackNumber: 5,
    length: "4:37",
    album: evol,
    published: false,
  });

  await mongoose.connection.close();
};

run().catch(console.error);