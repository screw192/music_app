const path = require('path');
const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require("../models/Album");
const Artist = require("../models/Artist");
const roleCheck = require("../middleware/roleCheck");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

const router = express.Router();

router.get('/', roleCheck, async (req, res) => {
  try {
    let albums;
    let artistName;

    switch (req.role) {
      case "admin":
        if (req.query.artist) {
          albums = await Album.find({artist: req.query.artist}).sort({year: 1});
          artistName = await Artist.findOne({_id: req.query.artist}).select("name");
        } else {
          albums = await Album.find();
        }
        break;

      case "user":
        if (req.query.artist) {
          albums = await Album.find({artist: req.query.artist, published: "true"}).sort({year: 1});
          artistName = await Artist.findOne({_id: req.query.artist}).select("name");
        } else {
          albums = await Album.find();
        }
        break;

      default:
        break;
    }

    res.send({
      artistName: artistName.name || null,
      albums: albums
    });
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const album =  await Album.findOne({_id: req.params.id}).populate("artist", "name");
    res.send(album);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', upload.single('cover'), async (req, res) => {
  try {
    const albumData = {
      title: req.body.title,
      year: req.body.year,
      artist: req.body.artist || null,
    };

    if (req.file) {
      albumData.cover = "uploads/" + req.file.filename;
    }

    const album = new Album(albumData);
    await album.save();

    res.send(album);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.patch("/:id", auth, permit("admin"), async (req, res) => {
  try {
    const albumID = req.params.id;

    const album = await Album.findOneAndUpdate({_id: albumID}, {...req.body}, {new: true})

    res.send(album);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete("/:id", auth, permit("admin"), async (req, res) => {
  try {
    const albumID = req.params.id;

    await Album.deleteOne({_id: albumID});

    res.send({message: "Album successfully deleted"});
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;