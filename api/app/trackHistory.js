const express = require("express");
const TrackHistory = require("../models/TrackHistory");
const User = require("../models/User");

const router = express.Router();

router.get("/", async (req, res) => {
  const token = req.get("Authorization");

  if (!token) {
    return res.status(401).send({error: "No token present"});
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.status(401).send({error: "User unauthorized"});
  }

  try {
    const trackHistory = await TrackHistory.find({user: user._id}).sort("-datetime").populate({
      path: "track",
      select: "title album",
      populate: {
        path: "album",
        select: "artist",
        populate: {
          path: "artist",
          select: "name",
        }
      }
    });

    res.send(trackHistory);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", async (req, res) => {
  const token = req.get("Authorization");

  if (!token) {
    return res.status(401).send({error: "No token present"});
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.status(401).send({error: "User unauthorized"});
  }

  try {
    const trackHistoryData = {};

    trackHistoryData.track = req.body.track;
    trackHistoryData.user = user._id;
    trackHistoryData.datetime = new Date();

    const trackHistory = new TrackHistory(trackHistoryData);
    await trackHistory.save();

    res.send(trackHistory);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;