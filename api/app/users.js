const express = require("express");
const multer = require("multer");
const path = require("path");
const axios = require("axios");
const {nanoid} = require("nanoid");
const config = require("../config");
const User = require("../models/User");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

const router = express.Router();

router.post("/", upload.single("avatarImage"), async (req, res) => {
  try {
    const userData = {
      username: req.body.username,
      password: req.body.password,
      displayName: req.body.displayName,
    };

    if (req.file) {
      userData.avatarImage = "uploads/" + req.file.filename;
    }

    const user = new User(userData);

    user.generateToken();
    await user.save();
    return res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post("/sessions", async (req, res) => {
  const user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(401).send({error: "Wrong username or password"});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({error: "Wrong username or password"});
  }

  user.generateToken();
  await user.save();

  return res.send({message: "Username and password are correct!", user});
});

router.delete("/sessions", async (req, res) => {
  const token = req.get("Authorization");
  const success = {message: "Success"};

  if (!token) return res.send(success);

  const user = await User.findOne({token});

  if (!user) return res.send(success);

  user.generateToken();

  await user.save();

  return res.send(success);
});

router.post('/facebookLogin', async (req, res) => {
  const inputToken = req.body.accessToken;

  const accessToken = config.facebook.appId + "|" + config.facebook.appSecret;

  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);

    if (response.data.data.error) {
      return res.status(401).send({message: "Facebook token incorrect"});
    }

    if (response.data.data["user_id"] !== req.body.id) {
      res.status(401).send({global: "Facebook token incorrect"});
    }

    let user = await User.findOne({username: req.body.id});
    console.log("user", user);

    if (!user) {
      console.log("!user");
      user = new User({
        username: req.body.id,
        password: nanoid(),
        displayName: req.body.name,
        avatarImage: req.body.picture.data.url,
      });
    }

    user.generateToken();
    await user.save();

    res.send({message: "Success", user});
  } catch (e) {
    res.status(401).send({global: "Facebook token incorrect"});
  }
});

module.exports = router;