const express = require("express");
const Track = require("../models/Track");
const Album = require("../models/Album");
const Artist = require("../models/Artist");
const roleCheck = require("../middleware/roleCheck");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get("/", roleCheck, async (req, res) => {
  try {
    let tracks;
    let albumTitle;
    let artistName;

    switch (req.role) {
      case "admin":
        if (req.query.album) {
          tracks = await Track.find({album: req.query.album}).sort({trackNumber: 1});
          albumTitle = await Album.findOne({_id: req.query.album}).select("title artist");
          artistName = await Artist.findOne({_id: albumTitle.artist}).select("name");
        } else {
          tracks = await Track.find();
        }
        break;

      case "user":
        if (req.query.album) {
          tracks = await Track.find({album: req.query.album, published: "true"}).sort({trackNumber: 1});
          albumTitle = await Album.findOne({_id: req.query.album}).select("title artist");
          artistName = await Artist.findOne({_id: albumTitle.artist}).select("name");
        } else {
          tracks = await Track.find();
        }
        break;

      default:
        break;
    }

    res.send({
      artistName: artistName.name || null,
      albumTitle: albumTitle.title || null,
      tracks: tracks
    });
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", async (req, res) => {
  try {
    const trackData = {
      title: req.body.title,
      trackNumber: req.body.trackNumber,
      length: req.body.length,
      album: req.body.album || null,
    };

    const track = new Track(trackData);
    await track.save();

    res.send(track);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.patch("/:id", auth, permit("admin"), async (req, res) => {
  try {
    const trackID = req.params.id;

    const track = await Track.findOneAndUpdate({_id: trackID}, {...req.body}, {new: true})

    res.send(track);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete("/:id", auth, permit("admin"), async (req, res) => {
  try {
    const trackID = req.params.id;

    await Track.deleteOne({_id: trackID});

    res.send({message: "Track successfully deleted"});
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;