const path = require("path");
const express = require("express");
const multer = require("multer");
const {nanoid} = require("nanoid");
const config = require("../config");
const Artist = require("../models/Artist");
const roleCheck = require("../middleware/roleCheck");
const permit = require("../middleware/permit");
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

const router = express.Router();

router.get("/", roleCheck, async (req, res) => {
  try {
    let artists;

    switch (req.role) {
      case "admin":
        artists = await Artist.find();
        break;
      case "user":
        artists = await Artist.find({published: "true"});
        break;
      default:
        break;
    }

    res.send(artists);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, upload.single("photo"), async (req, res) => {
  try {
    const artistData = {
      name: req.body.name,
      info: req.body.info,
    };

    if (req.file) {
      artistData.photo = "uploads/" + req.file.filename;
    }

    const artist = new Artist(artistData);
    await artist.save();

    res.send(artist);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.patch("/:id", auth, permit("admin"), async (req, res) => {
  try {
    const artistID = req.params.id;

    const artist = await Artist.findOneAndUpdate({_id: artistID}, {...req.body}, {new: true})

    res.send(artist);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete("/:id", auth, permit("admin"), async (req, res) => {
  try {
    const artistID = req.params.id;

    await Artist.deleteOne({_id: artistID});

    res.send({message: "Artist successfully deleted"});
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;